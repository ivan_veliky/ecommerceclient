# Ecommerce

Simple e-commerce client built with Angular

## Before Start (Prerequisites)

You should clone (https://gitlab.com/ivan_veliky/ecommerceapi.git) <br>
and run webapi on localhost with 5001 port before running client server<br>
otherwise it wont show any data

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Note

you may need to install additional packages via npm (depends on what ng says)<br>
example: `npm install --save-dev @angular-devkit/build-angular`