import { Component, OnInit } from '@angular/core';
import { CatalogService } from '../catalog.service';
import { Category } from '../Category';

@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.css']
})
export class ToolbarComponent implements OnInit {

  private title: string = 'Ecommerce';
  categories: Category[];

  constructor(
    private catalogService: CatalogService,
  ) { }

  ngOnInit() {
    this.catalogService.getCategories().subscribe(cats => this.categories = cats)
  }

}
