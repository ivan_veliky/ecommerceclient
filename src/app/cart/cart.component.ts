import { Component, OnInit } from '@angular/core';
import { ITEMS } from '../MockCartItems';
import { CartService } from '../cart.service';
import { Cart } from '../Cart';
import { CartItem } from '../CartItem';
import { Product } from '../Product';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {

  constructor(
    private cartService: CartService,
  ) { }

  items: CartItem[] =[];
  

  ngOnInit() {
    this.getCart();
  }

  getCart(): void {
    this.cartService.getCartItems().subscribe(c => {
      this.items = c;
      console.log(c);
      
    });
  }

  removeItem(item: CartItem): void {
    this.cartService.removeItem(item).subscribe(removedItem => {
      if(removedItem != null && item.id == removedItem.id) {
        this.items = this.items.filter(i => i !== removedItem);
        this.getCart();
      } else {
        // add logging
      }
    });
  }

  AddItem(product: Product): void {
    this.cartService.AddItem(product).subscribe(item => {
      if(item!=null) {
        this.items.push(item);
        this.getCart();
      } else {
        // some logging
      }
    });
  }
}
