import { Category } from './Category';

export const CATS: Category[] = [
    {id:1, name: 'All'},
    {id: 11, name: "Laptops"},
    {id: 12, name: "Smartphones"},
];