import { Product } from './Product';

export const PRODUCTS: Product[] = [
    {id: 11, name: 'product1', description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit', price: 9.98, category: 'Laptops', pic: ''},
    {id: 12, name: 'product2', description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit', price: 9.98, category: 'Laptops', pic: ''},
    {id: 13, name: 'product3', description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit', price: 9.98, category: 'Laptops', pic: ''},
    {id: 14, name: 'product4', description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit', price: 9.98, category: 'Laptops', pic: ''},
    {id: 15, name: 'product5', description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit', price: 9.98, category: 'Smartphones', pic: ''},
    {id: 16, name: 'product6', description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit', price: 9.98, category: 'Smartphones', pic: ''},
    {id: 17, name: 'product7', description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit', price: 9.98, category: 'Smartphones', pic: ''},
    {id: 18, name: 'product8', description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit', price: 9.98, category: 'Smartphones', pic: ''},
];