import { Component, OnInit } from '@angular/core';
import { CatalogService } from '../catalog.service';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { Product } from '../Product';
import { CartService } from '../cart.service';

@Component({
  selector: 'app-product-details',
  templateUrl: './product-details.component.html',
  styleUrls: ['./product-details.component.css']
})
export class ProductDetailsComponent implements OnInit {

  product: Product;
  messages: string[] =[];

  constructor(
    private catalogService: CatalogService,
    private cartService: CartService,
    private route: ActivatedRoute,
    private location: Location,
  ) { }

  ngOnInit() {
    this.getProduct()
  }

  getProduct(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.catalogService.getProduct(id).subscribe(prod => this.product = prod)
  }

  goBack(): void {
    this.location.back();
  }

  addItem(product: Product): void {
    this.cartService.AddItem(product).subscribe(item => {
      if(item != null && item.product.id == product.id){
        this.addMessage("Product was added to cart");
        console.log(item);
        
      }else {
        this.addMessage("FAIL! try later");
      }
    });
  }

  clearMessages(): void {
    this.messages = [];
  }

  addMessage(message: string): void {
    this.messages.push(message);
  }

}
