import { Injectable } from '@angular/core';

import { PRODUCTS } from './MockProducts';
import { Observable, of, ObservableInput } from 'rxjs';
import { Product } from './Product';
import { Category } from './Category';
import { CATS } from './MockCats';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CatalogService {

  private baseUrl = 'https://localhost:5001/api';  // url to web api

  constructor(
    private http: HttpClient,
  ) { }

  products: Product[] = PRODUCTS;
  cats: Category[] = CATS;

  getProducts(): Observable<Product[]> {
    return this.http.get<Product[]>(this.baseUrl + '/products')
      .pipe(
        catchError(this.handleError<Product[]>('getProducts', []))
      );
  }

  getProduct(id: number): Observable<Product> {
    return this.http.get<Product>(this.baseUrl + '/products/' + id).pipe(
      catchError(this.handleError<Product>('getProduct', null))
    );
    ;
  }

  getCategories(): Observable<Category[]> {
    return this.http.get<Category[]>(this.baseUrl + '/categories/')
    .pipe(
      catchError(this.handleError<Category[]>('getCategories', []))
    );
  }

  getProductsByCategory(cat: string): Observable<Product[]> {
    return this.http.get<Product[]>(this.baseUrl + '/products/byCategory?name=' + cat)
    .pipe(
      catchError(this.handleError<Product[]>('getProductsByCategory', []))
    );
  }


  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error);
      return of(result as T);
    };
  }

}
