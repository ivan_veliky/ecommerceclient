import { PRODUCTS } from "./MockProducts";
import { CartItem } from './CartItem';

export const ITEMS: CartItem[] = [
    { id:1, cartId:1, quantity:1, product: PRODUCTS[0] },
    { id:2, cartId:1, quantity:1, product: PRODUCTS[1] },
];