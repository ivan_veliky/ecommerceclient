import { Injectable } from '@angular/core';

import { PRODUCTS } from './MockProducts';
import { Observable, of, ObservableInput } from 'rxjs';
import { Product } from './Product';
import { Category } from './Category';
import { CATS } from './MockCats';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';
import { Cart } from './Cart';
import { CartItem } from './CartItem';

@Injectable({
  providedIn: 'root'
})
export class CartService {

  private baseUrl = 'https://localhost:5001/api';  // url to web api

  constructor(
    private http: HttpClient,
  ) { }

  getCartItems(): Observable<CartItem[]> {
    return this.http.get<CartItem[]>(this.baseUrl + '/cart/1/getItems') // only 1 user yet
    .pipe(
      catchError(this.handleError<CartItem[]>('getCartItems', []))
    );
  }

  removeItem(item: CartItem): Observable<CartItem> {
    return this.http.delete<CartItem>(this.baseUrl + '/cart/1/removeItem/'+item.id) // only 1 user yet
    .pipe(
      catchError(this.handleError<CartItem>('removeItem', null))
    );
  }

  AddItem(product: Product): Observable<CartItem> {
    return this.http.post<CartItem>(this.baseUrl + '/cart/1/addItem/'+product.id, product.id) // only 1 user yet
    .pipe(
      catchError(this.handleError<CartItem>('addItem', null))
    );
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error);
      return of(result as T);
    };
  }
}
