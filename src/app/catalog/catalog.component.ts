import { Component, OnInit } from '@angular/core';
import { Product } from '../Product';
import { CatalogService } from '../catalog.service';
import { Router, ActivatedRoute } from '@angular/router';
import { strictEqual } from 'assert';

@Component({
  selector: 'app-catalog',
  templateUrl: './catalog.component.html',
  styleUrls: ['./catalog.component.css']
})
export class CatalogComponent implements OnInit {

  products: Product[] = [];
  category: string;

  constructor(
    private catalogService: CatalogService,
    private route: ActivatedRoute,
  ) { }

  ngOnInit() {
    this.getCatalog();
  }

  getCatalog(): void {
    this.category = this.route.snapshot.paramMap.get('category');
    if(this.category && this.category!='All') {
      this.catalogService.getProductsByCategory(this.category).subscribe(prods => this.products = prods);
    } else {
      this.catalogService.getProducts().subscribe(prods => this.products = prods);
    }
  }

}
