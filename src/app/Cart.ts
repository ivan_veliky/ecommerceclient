import { CartItem } from './CartItem';

export class Cart {
    id: number;
    total: number;
    cartItems: CartItem;
}