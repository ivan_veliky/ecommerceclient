import { Product } from './Product';

export class CartItem {
    id: number;
    cartId: number;
    quantity: number;
    product: Product;
}